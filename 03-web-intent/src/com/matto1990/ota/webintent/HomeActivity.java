package com.matto1990.ota.webintent;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void openGoogle(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://google.com/"));
        startActivity(intent);
    }

    public void openMyWebsite(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://matto1990.com/"));
        startActivity(intent);
    }
}
