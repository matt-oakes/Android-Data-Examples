require 'rake/clean'
require 'rexml/document'
require 'rubygems'
require 'zip/zip'
require 'find'
require 'fileutils'
require 'json'

include FileUtils
include Rake::DSL

CLEAN.include('bin', 'gen')

buildClass = Class.new do
    # Creates an apk using the default ant build task
    def build(type, projectName)
        puts "Building..."
        sh %{ant #{type}} do |ok, res|
            if (ok)
                fromFilename = "bin/#{projectName}-#{type}.apk"
                toFilename = "bin/#{projectName}.apk"
                mv(fromFilename, toFilename)

                puts 'Build Succesful'
                return true
            else
                puts 'Build Failed!'
                return false
            end
        end
    end
end
builder = buildClass.new

projectClass = Class.new do
    # Gets the name of the project from the ant file
    def getProjectName()
        return REXML::Document.new(File.new('build.xml')).root.attributes["name"]
    end

    # Gets the version code of the project
    def getVersionCode()
        return REXML::Document.new(File.new('AndroidManifest.xml')).root.attributes["android:versionCode"]
    end

    # Gets the version name of the project
    def getVersionName()
        return REXML::Document.new(File.new('AndroidManifest.xml')).root.attributes["android:versionName"]
    end

    # Get the package name from the android manifest from a specified folder
    def getPackageNameInFolder(folder)
        return REXML::Document.new(File.new(folder + 'AndroidManifest.xml')).root.attributes["package"]
    end

    # Get the package name from the android manifest from the root folder
    def getPackageName()
        getPackageNameInFolder('')
    end

    # Get the launcher activity from the android manifest
    def getLauncherActivity()
        return REXML::Document.new(File.new('AndroidManifest.xml')).root.elements["application/activity/intent-filter/category[@android:name='android.intent.category.LAUNCHER']"].parent.parent.attributes["android:name"]
    end

    # Load the properties from the given xml file
    def loadProperties(propertiesFilename)
        properties = {}
        File.open(propertiesFilename, 'r') do |propertiesFilename|
            propertiesFilename.read.each_line do |line|
                line.strip!
                if (line[0] != ?# and line[0] != ?=)
                    i = line.index('=')
                    if (i)
                        properties[line[0..i - 1].strip] = line[i + 1..-1].strip
                    else
                        properties[line] = ''
                    end
                end
            end
        end
        return properties
    end

    # Get the current sdk dir
    def getSdkDir()
        return loadProperties('local.properties')['sdk.dir']
    end

    # Get the dirrect build target
    def getBuildTarget()
        return loadProperties("project.properties")['target']
    end

    # Get the list of library project directories
    def getLibraryProjectDirectories()
        properties = loadProperties("project.properties")
        directories = []

        stop = false
        i = 1
        until stop
            if (properties['android.library.reference.' + i.to_s()] != nil)
                directories[i - 1] = properties['android.library.reference.' + i.to_s()]
            else
                stop = true
            end
            i += 1
        end

        return directories
    end
end
project = projectClass.new

adbClass = Class.new do
    # Install the specified type of build on the device
    def install(filename)
        sh %{adb install -r #{filename}} do |ok, res|
            if (ok)
                puts 'Install Succesful'
                return true
            else
                puts 'Install Failed'
                return false
            end
        end
    end

    # Install the application on the device
    def run(packageName, launcherActivity)
        sh %{adb shell am start -a android.intent.action.MAIN -n #{packageName}/#{launcherActivity}} do |ok, res|
            if (ok)
                puts 'Running Application'
                return true
            else
                puts 'Failed to Start Application'
                return false
            end
        end
    end
end
adb = adbClass.new

themeClass = Class.new do
    # A function that zips up a directory
    # source: the directory you wish to zip up
    # destination: the file name & path of where you want your zip to go
    def zipDirectory(source, destination)
        puts source
        puts destination
        puts pwd
        Zip::ZipFile.open(destination, Zip::ZipFile::CREATE) do |zipfile|
            Find.find(source) do |path|
                Find.prune if File.basename(path)[0] == ?.
                dest = /#{WORKING_DIRECTORY}\/(\w.*)/.match(path)
                zipfile.add(dest[1],path) if dest
            end
        end
    end

    def unzipFile(file, destination)
        Zip::ZipFile.open(file) do |zip_file|
            zip_file.each do |f|
                f_path = File.join(destination, f.name)
                FileUtils.mkdir_p(File.dirname(f_path))
                zip_file.extract(f, f_path) unless File.exist?(f_path)
            end
        end
    end
end
theme = themeClass.new

def ask message
    print message
    STDIN.gets.chomp
end

task :default => ['build:debug']

namespace 'build' do
    desc "Builds, installs and runs a debug version"
    task :debug => [:clean, :buildDebug, "adb:install", "adb:run"]

    desc "Builds, installs and runs a debug release"
    task :release => [:clean, :buildRelease, "adb:install", "adb:run"]

    desc "Build debug version"
    task :buildDebug do
        if not builder.build("debug", project.getProjectName())
            fail('Build Failed')
        end
    end

    desc "Build a release version"
    task :buildRelease do
        if not builder.build("release", project.getProjectName())
            fail('Build failed')
        end
    end
end

namespace "adb" do
    desc "Install the last built. Must have run a build before"
    task :install do
        filename = "bin/#{project.getProjectName()}.apk"
        if not adb.install(filename)
            fail('Install failed')
        end
    end

    desc "Runs the version of the application that's installed on the phone"
    task :run do
        packageName = project.getPackageName()
        launcherActivity = project.getLauncherActivity()
        if not adb.run(packageName, launcherActivity)
            fail()
        end
    end

    desc "Clears the data on the connected device"
    task :clearData do
        packageName = project.getPackageName()
        sh %{adb shell pm clear #{packageName}}
    end

    desc "Uninstalls the package from the device"
    task :uninstall do
        packageName = project.getPackageName()
        sh %{adb uninstall #{packageName}}
    end
end
