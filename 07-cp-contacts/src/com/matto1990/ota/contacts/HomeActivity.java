package com.matto1990.ota.contacts;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HomeActivity extends Activity {
    private static final String[] PROJECTION = new String[] {
        ContactsContract.Contacts._ID,
        ContactsContract.Contacts.DISPLAY_NAME,
        ContactsContract.Contacts.PHOTO_ID,
        ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
        ContactsContract.Contacts.PHOTO_URI
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        AutoCompleteTextView nameEditText = (AutoCompleteTextView) findViewById(R.id.name);

        Cursor contactsCursor = managedQuery(ContactsContract.Contacts.CONTENT_URI,
            PROJECTION,
            null,
            null,
            null);

        List<String> contacts = new ArrayList<String>();

        while (contactsCursor.moveToNext()) {
            String name = contactsCursor.getString(contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            if (name != null && !contacts.contains(name)) {
                contacts.add(name);
            }
        }

        Collections.sort(contacts);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, contacts);
        nameEditText.setAdapter(adapter);
    }
}
