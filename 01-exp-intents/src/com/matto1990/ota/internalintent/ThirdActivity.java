package com.matto1990.ota.internalintent;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ThirdActivity extends Activity {
    public static final String NUMBER_EXTRA = "com.matto1990.ota.internalintent.number";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.third);

        String numberString = getIntent().getStringExtra(NUMBER_EXTRA);
        TextView numberView = (TextView) findViewById(R.id.number);
        numberView.setText(numberString);
    }
}
