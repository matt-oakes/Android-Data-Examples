package com.matto1990.ota.internalintent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class HomeActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    // Send an intent to open a specific activity
    public void secondClick(View v) {
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }

    // Send an intent to open a specific activity with data
    public void thirdClick(View v) {
        Intent intent = new Intent(this, ThirdActivity.class);

        long randomNumber = Math.round(Math.random() * 10000);
        intent.putExtra(ThirdActivity.NUMBER_EXTRA, "" + randomNumber);

        startActivity(intent);
    }
}
