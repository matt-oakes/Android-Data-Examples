package com.matto1990.ota.internalintent;

import android.app.Activity;
import android.os.Bundle;

public class SecondActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second);
    }
}
