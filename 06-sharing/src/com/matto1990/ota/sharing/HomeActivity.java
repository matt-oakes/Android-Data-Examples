package com.matto1990.ota.sharing;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.ShareActionProvider;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;

public class HomeActivity extends Activity {
    private ShareActionProvider mShareActionProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        // This is only for ICS devices
        if (Build.VERSION.SDK_INT >= 14) {
            mShareActionProvider = (ShareActionProvider) menu.findItem(R.id.menu_share).getActionProvider();
            mShareActionProvider.setShareIntent(getShareIntent());
        }

        return true;
    }

    // This is for pre-ISC devices
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_share:
                startActivity(Intent.createChooser(getShareIntent(), "Share using..."));
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private Intent getShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.subject));
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.body));

        return intent;
    }
}
