package com.matto1990.ota.emailold;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class HomeActivity extends Activity {
    // This is a pattern matcher for email addresses. In API Level 8+ you can just use:
    //   Patterns.EMAIL_ADDRESS
    // This is the same pattern, just included here so we're compatable back to API level 5
    public static final Pattern EMAIL_ADDRESS
        = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"
        );

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the content view and get the email EditText
        setContentView(R.layout.main);
        AutoCompleteTextView emailEditText = (AutoCompleteTextView) findViewById(R.id.email);

        // Create an array list to store our emails
        List<String> emails = new ArrayList<String>();

        // Get all the accounts on the phone and loop through them
        Account[] accounts = AccountManager.get(this).getAccounts();
        for (Account account : accounts) {
            // Check if the name of the account is an email
            if (EMAIL_ADDRESS.matcher(account.name).matches()) {
                // If it is, we can add it to our array list
                String email = account.name;
                
                // Check if we already have this email
                if (!emails.contains(email)) {
                    emails.add(email);
                }
            }
        }

        // Create an array adapter and set the adapter on the email EditText
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, emails);
        emailEditText.setAdapter(adapter);
    }
}
