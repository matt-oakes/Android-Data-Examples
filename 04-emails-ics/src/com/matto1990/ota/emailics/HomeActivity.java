// Massive help on this one from Roman Nurik
//    http://stackoverflow.com/a/2175688/99582

package com.matto1990.ota.emailics;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {
    // The projection for the cursor
    private static final String[] EMAIL_PROJECTION = new String[] {ContactsContract.CommonDataKinds.Email.ADDRESS};

    // A reference to the email EditText
    private AutoCompleteTextView emailEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        emailEditText = (AutoCompleteTextView) findViewById(R.id.email);

        // Start our loading
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle arguments) {
        return new CursorLoader(this,
            // Get the profile data rows
            Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI, ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
            EMAIL_PROJECTION,
            // Only retreive email addresses
            ContactsContract.Contacts.Data.MIMETYPE + " = ?",
            new String[]{ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE},
            // We don't care about the order at all
            null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        // Create an array list to store our emails in
        List<String> emails = new ArrayList<String>();

        // If we have any, loop through all emails and add them to the ArrayList
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                // Get the email and add it to the ArrayList
                String email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                emails.add(email);
            }
        }

        // Create an array adapter and set it on the email EditText
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, emails);
        emailEditText.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
    }
}
